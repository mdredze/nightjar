import pathlib
from setuptools import setup

# The directory containing this file
HERE = pathlib.Path(__file__).parent

# The text of the README file
README = (HERE / "README.md").read_text()

# This call to setup() does all the work
setup(
    name="nightjar",
    version="1.0.10",
    description="Synthetic replacement anonymization of social media data",
    long_description=README,
    long_description_content_type="text/markdown",
    author="Johns Hopkins University",
    license="2-Clause BSD",
    packages=["nightjar"],
    include_package_data=True,
    install_requires=["faker", "spacy", "pycorenlp"],
    entry_points={
        "console_scripts": [
            "nightjar=nightjar.__main__:main",
        ]
    },
)