# Nightjar

Nightjar is an anonymization package for social media.  It uses synthetic replacement: sensitive entities are identified and replaced with similar, but fake values.  

**Disclaimer:** Nightjar is not intended to be a perfect solution.  Any text processed through Nightjar should still be reviewed by a human trained in removing sensitive data before being made public.  Please also note that synthetic replacement is not sufficient to prevent de-anonymization of many publicly-available social media posts.

### Prerequisites

Requires Python 3.7.3 or higher

### Installing

Installing through pip will install all dependency packages as well.

```
pip install nightjar
```

### Usage

Nightjar allows you to choose between spaCy (https://spacy.io/) and CoreNLP (https://github.com/smilli/py-corenlp) for the anonymization.  SpaCy is the default.

**spaCy instructions:**

```
import nightjar

text = "..."
nightjar.replace(text)
```

**CoreNLP instructions:**
CoreNLP must be running on a local server on port 9000.  Download CoreNLP here: https://stanfordnlp.github.io/CoreNLP/index.html#download then change directories into the downloaded folder and run:

`java -mx4g -cp "*" edu.stanford.nlp.pipeline.StanfordCoreNLPServer -port 9000 -timeout 15000`

The server must be running for nightjar to work.
```
import nightjar

text = "..."
nightjar.replace(text, package='corenlp')
```

## Command Line Usage
Nightjar can also be run from the command line to process json files containing text.  Pass the filepath to the input json file and the desired output filepath (should be .txt).  Use the --spacy or --corenlp flags to select the process for anonymization.  For corenlp, the server must be running as described above.  

```nightjar input_data.json output_file.txt --spacy```

## Author

Johns Hopkins University

## License

This project is licensed under the 2-Clause BSD License
