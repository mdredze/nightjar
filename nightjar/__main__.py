from faker import Faker
import sys
import json
import argparse
import os
import re

# postal code XXXXX, XXXXX-XXXX
zip_code_pattern = re.compile(r"""\b(\d{5}(-\d{4})?)\b""")

# SSN/PHONE/FAX XXX-XX-XXXX, XXX-XXX-XXXX, XXX-XXXXXXXX, etc.
phone_pattern = re.compile(r"(\d{3}[-\.\s]??\d{3}[-\.\s]??\d{4}|\(\d{3}\)\s*\d{3}[-\.\s]??\d{4}|\d{3}[-\.\s]??\d{4})")

# IDs
id_pattern = re.compile(r"\b([A-Z0-9\-/]{6}[A-Z0-9\-/]*)\b")

twitter_username = re.compile("@(\w){1,15}")

def replace_combined(text):
    from pycorenlp import StanfordCoreNLP
    import spacy
    corenlp_model = StanfordCoreNLP('http://localhost:9000')
    spacy_model = spacy.load('en')
    fake = Faker()
    message = re.sub(phone_pattern, fake.phone_numbeR(), text)
    message = spacy_model(message)
    sanatized_tokens = []
    previous_entity = '0'
    for token in message:
        corenlp_result = corenlp_model.annotate(token.string, properties={
            'annotators': 'ner',
            'outputFormat': 'json',
            'timeout': 10000,
            'tokenize.invertible': 'true'
        })
        corenlp_ner = corenlp_result['sentences'][0]['tokens'][0]['ner'] if len(corenlp_result['sentences']) and len(corenlp_result['sentences'][0]['tokens']) else '0'
        if re.search(zip_code_pattern, token.string):
            sanatized_tokens.append(fake.postcode() + token.whitespace_)
        elif token.like_email or corenlp_ner == 'EMAIL':
            sanatized_tokens.append(fake.email() + token.whitespace_)
        elif token.like_url or corenlp_ner == 'URL':
            sanatized_tokens.append(fake.uri() + token.whitespace_)
        elif twitter_username.match(token.text) or corenlp_ner == 'HANDLE':
            sanatized_tokens.append('@' + fake.user_name() + token.whitespace_)
        elif corenlp_ner == 'PERSON':
            if not corenlp_ner == previous_entity:
                sanatized_tokens.append(fake.name() + token.whitespace_)
        elif token.ent_type_ == 'PERSON':
            if token.ent_iob == 3:
                sanatized_tokens.append(fake.name() + token.whitespace_)
        elif corenlp_ner == 'CITY':
            sanatized_tokens.append(fake.city() + token.whitespace_)
        elif corenlp_ner == 'STATE_OR_PROVINCE':
            sanatized_tokens.append(fake.state() + token.whitespace_)
        elif corenlp_ner == 'COUNTRY':
            sanatized_tokens.append(fake.country() + token.whitespace_)
        elif corenlp_ner == 'ORGANIZATION' or (token.ent_type_ == 'ORG' and token.ent_iob == 3):
            sanatized_tokens.append(fake.company() + token.whitespace_)
        elif token.ent_type_ == 'GPE':
            if token.ent_iob == 3:
                sanatized_tokens.append(fake.city() + token.whitespace_)
        elif token.ent_type_ == 'NORP':
            if token.ent_iob == 3:
                sanatized_tokens.append(fake.company() + token.whitespace_)
        elif token.ent_type_ == 'FAC':
            if token.ent_iob == 3:
                sanatized_tokens.append(fake.company() + token.whitespace_)
        elif token.ent_type_ == 'WORK_OF_ART':
            sanatized_tokens.append(fake.sentence(nb_words=3) + token.whitespace_)
        elif token.ent_type_ == 'EVENT':
            sanatized_tokens_tokens.append(fake.sentence(nb_words=2) + token.whitespace_)
        elif id_pattern.match(token.text):
            sanatized_tokens.append(fake.itin() + token.whitespace_)
        else:
            sanatized_tokens.append(token.string)
        previous_entity = corenlp_ner
    return "".join(sanatized_tokens) + '\n\n'
               
def replace_corenlp(text, nlp):
    fake = Faker()
    message = re.sub(phone_pattern, fake.phone_number(), text)
    truecase_result = nlp.annotate(message, properties={
        'annotators': 'truecase',
        'outputFormat': 'json',
        'timeout': 10000,
        'tokenize.invertible': 'true'
    })
    lowercased_text = ""
    for sentence in truecase_result['sentences']:
        words = [word['truecaseText'] for word in sentence['tokens']]
        lowercased_text = lowercased_text + "".join([word['truecaseText'] + word['after'] for word in sentence['tokens']])

    ner_result = nlp.annotate(lowercased_text, properties={
        'annotators': 'ner',
        'outputFormat': 'json',
        'timeout': 10000,
        'tokenize.invertible': 'true'
    })
    sanatized_tokens = []
    previousEntity = '0'
    for sentence in ner_result["sentences"]:
        for word in sentence['tokens']:
            entity = word['ner']
            if entity == 'EMAIL':
                sanatized_tokens.append(fake.email())
            elif re.search(zip_code_pattern, word['word']):
                sanatized_tokens.append(fake.postcode())
            elif entity == 'URL':
                sanatized_tokens.append(fake.uri())
            elif entity == 'HANDLE':
                sanatized_tokens.append('@' + fake.user_name())
            elif entity == 'PERSON':
                if not entity == previousEntity:
                    sanatized_tokens.append(fake.name())
            elif entity == 'ORGANIZATION':
                sanatized_tokens.append(fake.company())
            elif entity == 'CITY':
                sanatized_tokens.append(fake.city())
            elif entity == 'STATE_OR_PROVINCE':
                sanatized_tokens.append(fake.state())
            elif entity == 'COUNTRY':
                sanatized_tokens.append(fake.country())
            elif id_pattern.match(token.text):
                sanatized_tokens.append(fake.itin() + token.whitespace_)
            else:
                sanatized_tokens.append(word['word'])
            previousEntity = entity
    return " ".join(sanatized_tokens)

def replace_spacy(text, nlp):
    fake = Faker()
    (message, phone_count) = re.subn(phone_pattern, fake.phone_number(), text)
    message = nlp(message)
    sanatized_tokens = []
    for token in message:
        if re.search(zip_code_pattern, token.string):
            sanatized_tokens.append(fake.postcode() + token.whitespace_)
        elif token.like_email:
            sanatized_tokens.append(fake.email() + token.whitespace_)
        elif token.like_url:
            sanatized_tokens.append(fake.uri() + token.whitespace_)
        elif twitter_username.match(token.text):
            sanatized_tokens.append('@' + fake.user_name() + token.whitespace_)
        elif token.ent_type_ == 'PERSON':
            if token.ent_iob == 3:
                sanatized_tokens.append(fake.name() + token.whitespace_)
        elif token.ent_type_ == 'ORG':
            if token.ent_iob == 3:
                sanatized_tokens.append(fake.company() + token.whitespace_)
        elif token.ent_type_ == 'GPE':
            if token.ent_iob == 3:
                sanatized_tokens.append(fake.city() + token.whitespace_)
        elif token.ent_type_ == 'NORP':
            if token.ent_iob == 3:
                sanatized_tokens.append(fake.company() + token.whitespace_)
        elif token.ent_type_ == 'FAC':
            if token.ent_iob == 3:
                sanatized_tokens.append(fake.company() + token.whitespace_)
        elif token.ent_type_ == 'WORK_OF_ART':
            sanatized_tokens.append(fake.sentence(nb_words=3) + token.whitespace_)
        elif token.ent_type_ == 'EVENT':
            sanatized_tokens_tokens.append(fake.sentence(nb_words=2) + token.whitespace_)
        elif id_pattern.match(token.text):
            sanatized_tokens.append(fake.itin() + token.whitespace_)
        else: sanatized_tokens.append(token.string)
    return "".join(sanatized_tokens)

def replace(text, package='spacy'):
    if package == 'corenlp':
        from pycorenlp import StanfordCoreNLP
        nlp = StanfordCoreNLP('http://localhost:9000')
        return replace_corenlp(text, nlp)
    elif package == 'spacy':
        import spacy
        nlp = spacy.load('en')
        return replace_spacy(text, nlp)
    else:
        return replace_combined(text)

def main():
    parser = argparse.ArgumentParser(description="Replace personally identifying words with similar fakes")
    parser.add_argument('input_file', metavar="Input_File", type=str, help="Path to json file containing formatted message data")
    parser.add_argument('output_file', metavar="Output_File", type=str, help="Path for output file containing sanatized messages")
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--spacy', action='store_true', help="Use spaCy NER")
    group.add_argument('--corenlp', action='store_true', help='Use Stanford coreNLP NER')
    args = parser.parse_args()

    if args.spacy:
        import spacy
        nlp = spacy.load('en')
        with open(args.input_file, "r") as input_file:
            output_file = open(args.output_file, "w")
            json_data = json.load(input_file)
            for item in json_data:
                if not item == None and not item['message'] == None:
                    output_file.write(item['message'] + '\n')
                    clean_message = replace_spacy(item['message'], nlp)
                    output_file.write(clean_message)
            output_file.close()
    elif args.corenlp:
        from pycorenlp import StanfordCoreNLP
        nlp = StanfordCoreNLP('http://localhost:9000')
        with open(args.input_file, "r") as input_file:
            output_file = open(args.output_file, "w")
            json_data = json.load(input_file)
            for item in json_data:
                if not item == None and not item['message'] == None:
                    output_file.write(item['message'] + '\n')
                    anonymized_message = replace_corenlp(item['message'], nlp)
                    if not anonymized_message == None:
                        output_file.write(anonymized_message + '\n')
                    else:
                        output_file.write("\n")
            output_file.close()

if __name__ == "__main__":
    main()